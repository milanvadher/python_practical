# Define fuction
def matrixMult():
    # for Matrix1
    print("\nEnter row and column for Matrix 1:") 
    r1 = int(input("Enter the no. of row = "))
    c1 = int(input("Enter the no. of Column = "))
    
    # declare Matrix1
    Matrix1 = [[0 for x in range(c1)] for y in range(r1)] # input from user

    # for Matrix2
    print("\nEnter row and column for Matrix 2:")
    print("Note: Matrix 1 column & Matrix 2 row must be same") 
    r2 = int(input("Enter the no. of row = "))
    c2 = int(input("Enter the no. of column = "))

    if(c1 == r2):
        # Matrix1: input value from user
        print("\nEnter value for Matrix 1:")
        for i in range(0, r1):
            for j in range(0, c1):
                print("Enter the value of (", i, " , ", j, ") = ", end="")
                Matrix1[i][j] = int(input())

        # print Matrix1
        print("Matrix 1 = ", Matrix1)

        # declare Matrix2    
        Matrix2 = [[0 for x in range(c2)] for y in range(r2)] # input from user

        # Matrix2: input value from user
        print("\nEnter value for Matrix 2:")
        for i in range(0, r2):
            for j in range(0, c2):
                print("Enter the value of (", i, " , ", j, ") = ", end="")
                Matrix2[i][j] = int(input())

        # declare Matrix3    
        Matrix3 = [[0 for x in range(c2)] for y in range(r1)] # store result 
        
        # print Matrix2
        print("Matrix 2 = ", Matrix2)

        # Matrix multiplication logic
        for i in range(0, r1):
            for j in range(0, c2):
                for k in range(0, r2):
                    Matrix3[i][j] += Matrix1[i][k] * Matrix2[k][j]

        # Print output
        print("\nMultiplication of Matrix1 & Matrix2 = ", end="")
        print(Matrix3)

    else:
        print("Matrix1 column & Matrix2 row must be same to perform multiplication of matrix")


# call function
matrixMult()