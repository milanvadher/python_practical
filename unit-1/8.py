from fractions import gcd
X = int(input("Enter the first number: "))
Y = int(input("Enter the second number: "))
ans = gcd(X,Y)
print("GCD of ",X," & ",Y," = ",ans)