#WAP script to print a dictionary where the keys are no. between 1-15 (both included) and the values are the square of key.
# dict = {};
# for i in range(1,16):
#     dict[i] = i*i
# print(dict)


# #WAP to sort a dictionary by key.
import collections

d = {2:3, 1:89, 4:5, 3:0, 6:5}
od = collections.OrderedDict(sorted(d.items()))
print(d)
print(od)

# rd = {}

# for key,value in d.items():
#     if value not in rd.values():
#         rd[key] = value

# print(rd)
