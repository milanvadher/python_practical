# N stone problem
try:
    N = int(input())
    assert 0 < N <= 100
except AssertionError:
    pass

MAX = 100

def findState(position):
    # 0 means losing state
    # 1 means wining state
    position[1] = 0
    position[2] = 1
    position[3] = 1
    position[4] = 1
    position[5] = 1
    position[6] = 1
    position[7] = 0

    for i in range(8,MAX+1):
        if not(position[i-2]) or not(position[i-3]) or not(position[i-5]):
            position[i] = 1
        else:
            position[i] = 0
    
position = [1] * (MAX+1)

findState(position)

if(position[N] == 1):
    print("Alice")
else:
    print("Bob")