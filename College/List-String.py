# String 
print ("1. String")
print()
str = "Hello"
print ("str:  ",str)
print ("str[0]: ",str[0])
print ("str[2:5]: ",str[2:5])
print ("str[2:]: ",str[2:])
print ("str *2: ",str *2)
print ('str + "Test": ' ,str + "Test")

print()
print()

#List
print ("2. List")
print()
list = [1,2,3,4]
print ("list: ",list)
print ("list[0]: ",list[0])
print ("list[-1]: ", list[-1])
print ("list[2:5]: ",list[2:5])
list.append('Milan')
print ("list[:]: ",list[:])
list[2:3] = [9,10]
print ("list[:]: ",list[:])

print()
print()

#dictionary
print ("3. Dictionary")
print()
dict = {}
