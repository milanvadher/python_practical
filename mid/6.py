def binarySearch(list,search):
    first = 0
    last = len(list)-1
    found = False

    while first <= last and not found:
        midpoint = (first + last) // 2
        if(list[midpoint] == search):
            found = True
        else:
            if(search < list[midpoint]):
                last = midpoint - 1
            else:
                last = midpoint + 1
        return found

n = int(input("Enter any no."))
list = [1,2,3,5,85,25,12]

print(binarySearch(list,n))