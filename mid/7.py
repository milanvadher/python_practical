class Employee(object):
    
    def __init__(self):
        super(Employee,self).__init__()
    
    def getData(self,name,dep,sal):
        self.n = name
        self.s = sal
        self.d = dep
    
    def printData(self):
        print("Name of Employee: ",self.n)
        print("Department of Employee: ",self.d)
        print("Salary of Employee: ",self.s)

emp = Employee()

name = input("Enter name = ")
dep = input("Enter department = ")
sal = input("Enter Salary = ")

emp.getData(name,dep,sal)
emp.printData()