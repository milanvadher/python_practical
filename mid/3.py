with open("google.txt",'r')as file: # open file in read mode
    words = file.read(); # read file
    print(words) # print file
    newWord = words.replace(".", "") # remove all '.' you can also remove ',' , '"" etc. all symbols
    print(newWord) # print file without '.'
    line = newWord.split() # saperate all the words 
    print(line) # print saperate words 
    uniq = set([x for x in line if line.count(x) == 1]) # find unique words and append in set
    uniqfile = list(uniq) # convert set into list
    print(uniqfile) # print list