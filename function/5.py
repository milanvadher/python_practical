import math

C = 50
H = 30

def output(D):
    Q = math.sqrt((2*C*D)/H)
    print(int(Q), end=',')

n = input('Enter your value: ')
n = [int(x) for x in n.split(',')]

for i in range(len(n)):
    output(n[i])
