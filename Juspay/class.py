# ------------------------------------------ Problem statement ---------------------------------------- #
# {
    # Traffic Lights: Minimum Time

    # You are given a roadmap of a country consisting of N cities and M roads. Each city has
    # traffic light. The traffic light has only 2 colors, Green and Red. All the traffic lights will
    # switch their color from Green to Red and vice versa after every T seconds. You can cross
    # city only when the traffic light in the city is Green. Initially, all the traffic lights are Green.
    # At a city, if the traffic light is Red, you have to wait for it to switch its color to Green. The
    # time taken to travel through any road is C seconds. Find the minimum amount of time (in
    # seconds) required to move from city 1 to city N.
    # It is guaranteed that the given roadmap will be connected. Graph won't contain multiple
    # edges and self loops.

    # Input

    # The first line contains 4 space separated integers, N(1 <= N <= 10>^3), M(N-1 <= M <= N(N-1)/2), T
    # (1 <= T <= 10^3) and C(1 <= C <= 10^3). Next M lines contains two integers each, U and V denoting
    # that there is a bidirectional road between U and V.

    # Output

    # Print the minimum amount of time (in seconds) required to move from city 1 to City N.

    # Note for sample input

    Fastest path will be 1->2->5. You can reach city 2 in 5 seconds. After 3 seconds the
    traffic light in City 2 will turn Red. So in city 2, you have to wait for 1 second for the traffic
    light to turn Green. So total time will be 5 seconds (from city 1 to city 2) + 1 second
    (waiting time at city 2) + 5 seconds (from city 2 to city 5) = 11 seconds.

    # Sample input
    # 5 5 3 5
    # 1 2
    # 1 3
    # 2 4
    # 1 4
    # 2 5
    # Sample Output
    # 11
# }
# ------------------------------------------ Problem statement ---------------------------------------- #

# input from user 
list = []
ans = []
N, M, T, C = input().split(" ")
# M time input from user U V
for i in range(0,int(M)):
    temp = input().split(" ")
    list.append(temp)
print (list)

start = 1
sum = 0

# find min distance logic 
for i in range(0,len(list)):
    if(int(list[i][0]) == start):
        sum = sum + int(C)
        print(sum)
        if(start == 4):
            print()
        else:
            while(sum % int(T) != 0):
                sum = sum + 1
        ans.append(sum)
        print(ans)
        start = int(list[i][1])

# addition of ans list's elements
add = 0
for i in range(0,len(ans)):
    add = add + int(ans[i])
print(add)
